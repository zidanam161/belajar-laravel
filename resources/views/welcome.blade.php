@extends('layout.master')

@section('judul')
Welcome
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{ $firstname }} {{ $lastname }}</h1>
    <h4>Terimakasih telah bergabung di Website kami. Media Belajar kita bersama!</h4>
@endsection