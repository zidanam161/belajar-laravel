@extends('layout.master')

@section('judul')
Edit Cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="inputName">Name</label>
      <input name="nama" type="text" class="form-control" id="inputName" value="{{$cast->nama}}">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="inputUmur">Umur</label>
      <input name="umur" type="text" class="form-control" id="inputUmur" value="{{$cast->umur}}">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="inputBio">Bio</label>
      <textarea name="bio" class="form-control" id="inputBio" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection