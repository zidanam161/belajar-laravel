@extends('layout.master')

@section('judul')
Detail {{$cast->nama}}
@endsection

@section('content')
<h1>{{$cast->nama}}</h1>
<p>Umur: {{$cast->umur}}</p>
<p>Bio: {{$cast->bio}}</p>
@endsection