<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('form');
    }

    public function greeting(Request $request) {
        $firstname = $request['fname'];
        $lastname = $request['lname'];
        return view('welcome', compact('firstname', 'lastname'));
    }
}
