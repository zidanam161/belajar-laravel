@extends('layout.master')

@section('judul')
Create Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="inputName">Name</label>
      <input name="nama" type="text" class="form-control" id="inputName">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="inputUmur">Umur</label>
      <input name="umur" type="text" class="form-control" id="inputUmur">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="inputUmur">Bio</label>
      <textarea name="bio" class="form-control" id="inputBio" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection